# CPP-QA: Static Analysis Tools for C/C++
This [Docker](https://www.docker.com) image provides some static analysis tools for C/C++ that I use for many of my projects.

## Supported platforms and PHP versions
This [Docker](https://www.docker.com) image is based on the Alpine image.

## Available Tools
* cppcheck - [CPP Checker](https://github.com/danmar/cppcheck/)

## How to install
TBD

## How to use

```shell
docker run -t -v $(pwd):/src registry.gitlab.com/stelgenhof/qa/cpp-qa
```
