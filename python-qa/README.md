# Python-QA: Static Analysis Tools for Python
This [Docker](https://www.docker.com) image provides some static analysis tools for Python that I use for many of my projects.

## Supported platforms and Python versions
This [Docker](https://www.docker.com) image is based on the Python 3 Alpine image.

## Available Tools
* cppcheck - [CPP Checker](https://github.com/danmar/cppcheck/)

## How to install
TBD

## How to use

```shell
docker run -t -v $(pwd):/src registry.gitlab.com/stelgenhof/qa/cpp-qa
```
